
import java.util.Arrays;

public class Task6 {

    /*Napisz metodę, która jako argument przyjmuje tablicę Stringów. Jako wynik ma
    zwracać tablice tej samej długości, w której wyrazy są napisane małymi literami(
            duże litery zamieniamy na małe).*/

    public static void main(String[] args) {

        String[] words = {"PiEs","kOt","SamoLoT","SaMocHód"};
        toLowerCase(words);
    }
    public static void toLowerCase(String[] words){

        System.out.println(Arrays.toString(words).toLowerCase());
    }
}
