public class Task7 {

/*Napisz metodę, która jako argument przyjmuje tablice Stringów. Jako wynik
metoda ma zwracać liczbę całkowitą, która oznacza najdłuższy wyraz w tablicy.*/

    public static void main(String[] args) {

        String[] words = {"pies", "kot", "samolot", "samochód", "lotniskowiec", "piłka"};
        System.out.println("Najdłuższy wyraz w tablicy ma " + theLongestWord(words) + " znaków");

    }

    public static int theLongestWord(String[] words) {

        int lenghtOfWord = 0;

        for (String word : words) {

            if (word.length() > lenghtOfWord) {

                lenghtOfWord = word.length();

            }
        }
        return lenghtOfWord;
    }
}
