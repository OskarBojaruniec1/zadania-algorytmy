/*Zadanie 27.
Napisz metodę, która sprawdza czy wyraz jest peselem. Dla uproszczenia
sprawdzamy czy każdy znak jest cyfrą i czy wyraz ma 11 znaków.
isPesel(“87122508076”)*/

public class Task21 {

    public static void main(String[] args) {

        System.out.println(isPesel("87122508076"));

    }

    public static boolean isPesel(String pesel) {

        char[] peselToChars = pesel.toCharArray();

        for (char c : peselToChars) {

            if (!Character.isDigit(c)) return false;

        }
        return pesel.length() == 11;
    }
}
