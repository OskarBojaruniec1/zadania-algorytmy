/*Zadanie 20.
Napisz metodę, która zwraca sumę dwóch stringów.
sum(“aaa”,”bb”) powinno zwrócić “aaabb”*/

public class Task14 {

    public static void main(String[] args) {

        System.out.println(stringsTogether("aaa", "bb"));
    }

    public static String stringsTogether(String a, String b) {

        return a + b;
    }
}
