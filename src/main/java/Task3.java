import java.util.Arrays;

public class Task3 {

    /*  Zadanie 8.
Napisz metodę, która zwraca posortowaną tablicę liczb.
mySort([4,1,9,15]) = [1,4,9,15]*/
    public static void main(String[] args) {

        int[] numbers = {4,1,9,15};
        System.out.println(Arrays.toString(mySort(numbers)));

    }

    public static int[] mySort(int[] numbersToSort) {

        for (int j = 0; j < numbersToSort.length - 1; j++) {

            for (int i = 0; i < numbersToSort.length - 1 - j; i++) {

                int positionX = numbersToSort[i];

                if (positionX > numbersToSort[i + 1]) {

                    numbersToSort[i] = numbersToSort[i + 1];
                    numbersToSort[i + 1] = positionX;

                }
            }
        }

    return numbersToSort;
    }

}
