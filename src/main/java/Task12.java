/*
Zadanie 17.
Napisz metodę sprawdzającą czy liczba jest parzysta.*/

public class Task12 {

    public static void main(String[] args) {

        System.out.println(isEvenNumber(24));
    }
    public static boolean isEvenNumber(int number){

        return number % 2 == 0;
    }
}
