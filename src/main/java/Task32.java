/*
Zadanie 10.
Napisz program który zlicza wszystkie liczby od 0 do N
 */

public class Task32 {

    public static void main(String[] args) {

        int n = 6;
        int sum = 0;

        for (int i = 0; i <= n; i++) {

            sum = sum + i;

        }

        System.out.println(sum);
    }
}
