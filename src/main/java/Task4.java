import java.util.Arrays;

public class Task4 {

/*Zadanie 9.
Napisz metodę, która zwraca tablice elementów środkowych.
makeMiddle([1,2,3,4]) = [2, 3]
Zakładamy, że długość tablicy jest zawsze podzielna przez 2.*/

    public static void main(String[] args) {

        int[] numbers = {1,2,3,4};
        System.out.println(Arrays.toString(makeMiddle(numbers)));

    }

    public static int[] makeMiddle(int[] numbers) {

        int middle = numbers.length/2;

        return new int[]{numbers[middle - 1],numbers[middle]};
    }
}
