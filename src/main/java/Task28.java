/*Zadanie 6.
Sprawdź czy podany wyraz jest palidromem czytany od przodu i od tyłu jest taki sam np. kajak, ala, oko
 */

public class Task28 {

    public static void main(String[] args) {

        String word = "pies";
        StringBuilder reversedWord = new StringBuilder(word).reverse();

        if (reversedWord.toString().equals(word)) System.out.println("Wyraz jest palindromem");
        else System.out.println("Wyraz nie jest palindromem");

    }
}
