/*Zadanie 26.
Napisz metodę, która sprawdza czy dany wyraz jako argument jest palindromem.*/

public class Task20 {

    public static void main(String[] args) {

        System.out.println(isPalindrome("kajak"));

    }

    public static boolean isPalindrome(String word) {

        StringBuilder wordToReverse = new StringBuilder(word).reverse();

        return wordToReverse.toString().equals(word);
    }
}
