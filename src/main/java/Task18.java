/* Zadanie 24.
Napisz funkcję, która stwierdza, czy zadana jako parametr liczba całkowita jest
liczbą pierwszą.*/

public class Task18 {

    public static void main(String[] args) {

        System.out.println(isPrimeNumber(613));

    }

    public static boolean isPrimeNumber(int number) {

        int divider = number;
        int counter = 0;
        int i = 0;
        boolean isPM = true;

        while (i < number) {

            if (number % divider == 0) counter++;

            if (counter > 2) {

                isPM = false;
            }
            divider--;
            i++;
        }
        return isPM;
    }
}
