/*Zadanie 15.
Napisz metodę, która wyznaczy liczbę wystąpień liczby k w danym ciągu.
numberOfElements([1,2,3,3,3,4,3],3) = 4 ( 3 występuje 4 razy w danym ciągu).
numberOfElements([1,2,3,3,3,4,3],8) = 0 ( 8 nie występuje ani razu w danym ciągu).*/

public class Task10 {

    public static void main(String[] args) {

        int[] numbers = {1, 2, 3, 3, 3, 4, 3};
        numberOfElements(numbers, 3);
    }

    public static void numberOfElements(int[] numbers, int k) {

        int counter = 0;

        for (int number : numbers) {

            if (number == k) {

                counter++;
            }
        }

        if (counter > 0) System.out.println(k + " występuje " + counter + " razy w danym ciągu");
        else System.out.println(k + " nie występuje ani razu w danym ciągu");
    }
}
