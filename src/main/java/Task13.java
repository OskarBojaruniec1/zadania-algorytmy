/*Zadanie 19.
Napisz funkcję, która stwierdza, czy zadana jako parametr liczba całkowita jest
kwadratem pewnej liczby całkowitej. Liczby będące kwadratami liczb całkowitych
to 1, 4, 9, 16 itd. Wartością funkcji ma być prawda, jeśli liczba spełnia warunek oraz
fałsz w przeciwnym wypadku.*/

public class Task13 {
    public static void main(String[] args) {

        System.out.println(isSquare(2));
    }

    public static boolean isSquare(double number) {

        double a = Math.sqrt(number);
        return a * a == number;
    }
}
