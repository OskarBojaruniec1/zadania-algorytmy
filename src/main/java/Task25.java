/*Zadanie 3.
Napisz program który pobiera N liczb całkowitych i wyświetla największą i najmniejszą z nich

int[] numbers = {1, 1, 4, ,6 ,6 ,90, 151, -6}
Najmniejsza podana liczba to -6, a największa to 151

 */

public class Task25 {

    public static void main(String[] args) {

        int[] numbers = {1, 1, 4, 6, 6, 90, 151, -6};
        minMaxNumber(numbers);

    }

    public static void minMaxNumber(int[] numbers) {

        int max = numbers[0];
        int min = numbers[0];

        for (int number : numbers) {

            max = Math.max(max, number);
            min = Math.min(min, number);
        }

        System.out.println("Najmniejsza podana liczba to " + min + ", a największa to " + max);
    }
}
