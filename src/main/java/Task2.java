/*
Zadanie 7.
Co z tym kodem jest nie tak?
int[] array = new int[5];
for (int i=0;i <=5;i++){
array[i] =12;
}*/

public class Task2 {

    public static void main(String[] args) {

        // out of bounds ponieważ nie da sie dodać na index 5 gdyż ten nie istnieje, wystarczy zmienić i <= 5 na i < 5
        int[] array = new int[5];
        for (int i = 0; i < 5; i++) {
            array[i] = 12;
        }
    }
}