import java.util.ArrayList;

/*Napisz metodę, która jako argument zawiera tablicę elementów typu boolean.
Metoda ma zwrócić nową tablicę, która zawiera tyle elementów true ile znajduje
się w bazowej tablicy.
newArray([false,true,true,false,false]) = [true,true]
newArray([true,true,true,true,false,false, true]) = [true,true,true,true,true]*/

public class Task9 {

    public static void main(String[] args) {

        boolean[] array = {true, true, true, true, false, false, true};
        System.out.println(newArray(array));
    }

    public static ArrayList<Boolean> newArray(boolean[] array) {

        ArrayList<Boolean> arrayOfTrue = new ArrayList<>();
        for (boolean b : array) {

            if (b) {
                arrayOfTrue.add(b);
            }
        }
        return arrayOfTrue;
    }
}
