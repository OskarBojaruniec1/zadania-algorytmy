/*Zadanie 5.
wypisz liczby od 1 do 100 następnie dla liczb podzielnych przez 3 wypisz Fizz dla liczb podzielnych
przez 5 wypisz Buzz dla liczb podzielnych przez 3 i przez 5 wypisz FizzBuzz,
w przeciwynym razie wypisz wartość liczby

 */
public class Task27 {

    public static void main(String[] args) {

        for (int i = 1; i <= 100; i++) {

            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
                continue;
            }
            if (i % 3 == 0) {
                System.out.println("Fizz");
                continue;
            }
            if (i % 5 == 0) {
                System.out.println("Buzz");
                continue;
            }
            System.out.println(i);

        }
    }
}
