import java.util.ArrayList;


/*Zadanie 4.
Jak usunąć za pomocą jednego przejścia po liście wszystkie duplikaty, napisz stosowny program

 */
public class Task26 {

    public static void main(String[] args) {

        int[] numbers = {33, 1, 2, 3, 3, 4, 1, 5, 6, 33};
        ArrayList<Integer> numbersWithoutDuplicates = new ArrayList<>();

        for (int number : numbers) {

            if (numbersWithoutDuplicates.contains(number)) continue;

            numbersWithoutDuplicates.add(number);

        }
        System.out.println(numbersWithoutDuplicates);

    }
}
