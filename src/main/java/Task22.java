/*
Zadanie 28.
Napisz program który odwróci tablicę intów
int[] numbers = {1,2,45,5,6,7,88,100}
int[] result = {100,88,7,6,5,45,2,1}
 */

import java.util.Arrays;

public class Task22 {

    public static void main(String[] args) {

        int[] numbers = {1, 2, 45, 5, 6, 7, 88, 100};
        int[] result = reverseArray(numbers);
        System.out.println(Arrays.toString(result));
    }

    public static int[] reverseArray(int[] numbers) {

        int[] reversedNumbers = new int[numbers.length];
        int j = numbers.length - 1;

        for (int i = 0; i < numbers.length; i++) {

            reversedNumbers[i] = numbers[j];
            j--;

        }
        return reversedNumbers;
    }

}
