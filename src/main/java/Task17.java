/*Zadanie 23.
Napisz funkcję, która stwierdza, czy zadana jako parametr liczba całkowita jest
sześcianem pewnej liczby naturalnej.*/

public class Task17 {

    public static void main(String[] args) {

        System.out.println(isCube(27));
    }
    public static boolean isCube(int number){

        double a = Math.cbrt(number);
        return a * a * a == number;
    }
}
