import java.util.ArrayList;
import java.util.Arrays;

/*
Zadanie 13.

Napisz program który za pomocą jednego przejscia znajdzie wszystke pary liczb,
których suma jest równa 0.
 */
public class Task35 {

    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1, 2, 4, 7, 8, 10, 10, -1, -2, -4, 100, 22, -10));
        ArrayList<Integer> result = new ArrayList<>();

        for (int number : numbers) {

            if (result.contains(number)) continue;
            if (numbers.contains(-(number))) {

                result.add(number);
                result.add(-(number));
            }
        }
        System.out.println(result);
    }
}
