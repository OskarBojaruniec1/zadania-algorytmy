/*
Zadanie 12.

Napisz program który wyszuka największą róznice pomiędzy liczbami w tablicy
 */
public class Task34 {

    public static void main(String[] args) {

        int[] numbers = {10, 3, 5, 6};
        System.out.println(BigDiff(numbers));

    }

    public static int BigDiff(int[] numbers) {
        int max = numbers[0];
        int min = numbers[0];

        for (int number : numbers) {

            max = Math.max(max, number);
            min = Math.min(min, number);
        }
        return max - min;
    }
}
