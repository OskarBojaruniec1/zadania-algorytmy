import java.util.Arrays;

public class Task1 {

/*Napisz metodę, która odwraca daną tablicę liczb całkowitych.
swap([1,2,3]) = [3,2,1]*/

    public static void main(String[] args) {

        int[] numbers = {1,2,3,4,5,6};
        System.out.println(Arrays.toString(swap(numbers)));

    }
    public static int[] swap (int[] numbers){

        int size = numbers.length;
        int[] swappedArray = new int[size];
        int index = 0;
        for (int i = numbers.length-1; i >= 0; i--){

            swappedArray[index] = numbers[i];
            index++;
        }
        return swappedArray;

    }
}
