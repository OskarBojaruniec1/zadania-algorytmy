import java.util.ArrayList;

public class Task5 {

/*Zadanie 10.
Napisz metodę, która jako argument przyjmuje tablicę Stringów. Jako wynik ma
zwracać tablice wszystkich słów, które zawierają słowa 5-literowe.
*/

    public static void main(String[] args) {

        String[] words = {"pies", "rower", "samolot", "chleb", "kot", "jajko"};
        System.out.println(getWordsWithFiveChars(words));
    }

    public static ArrayList<String> getWordsWithFiveChars(String[] words) {

        ArrayList<String> wordsWithFiveChars = new ArrayList<>();
        for (String word : words) {

            if (word.length() == 5) {
                wordsWithFiveChars.add(word);
            }
        }
        return wordsWithFiveChars;
    }
}
