/*
Zadanie 7.
Znajdź najdłuższy wyraz w podanym tekście np. Ala ma kota -> kota
 */
public class Task29 {

    public static void main(String[] args) {

        String sentence = "Ala ma kota";
        String[] splitedSentence = sentence.split(" ");

        String theLongestWord = "";

        for (String s : splitedSentence) {

            if (s.length() > theLongestWord.length()) theLongestWord = s;

        }

        System.out.println(theLongestWord);
    }
}
