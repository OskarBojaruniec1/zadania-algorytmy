/*
Zadanie 2.
Zaprogramuj algorytm sprawdzający czy dana liczba jest pierwsza

7 to jest liczba pierwsza
6 to nie jest liczba pierwsza
 */
public class Task24 {

    public static void main(String[] args) {

        System.out.println(isPrimeNumber(7));
    }

    public static boolean isPrimeNumber(int number) {

        int divider = number;
        int counter = 0;

        while (divider > 0) {

            if (number % divider == 0) counter++;
            divider--;
        }

        return counter <= 2;
    }
}
