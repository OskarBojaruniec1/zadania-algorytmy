/*Zadanie 16.
Napisz metodę, która ma trzy parametry formalne a, b, c będące liczbami
całkowitymi. Funkcja zwraca prawdę, jeśli zadane liczby są liczbami
pitagorejskimi oraz fałsz w przeciwnym wypadku. Liczby pitagorejskie spełniają
warunek: a*a+b*b=c*c.*/

public class Task11 {

    public static void main(String[] args) {

        System.out.println(arePythagoreanNumbers(7, 24, 25));

    }

    public static boolean arePythagoreanNumbers(int a, int b, int c) {

        return a * a + b * b == c * c;

    }
}
