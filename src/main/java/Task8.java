/* Napisz metodę, która jako argument przyjmuje tablicę liczb. Jako wynik metoda
powinna zwrócić tablicę wszystkich liczb, które mają dokładnie dwa dzielniki.*/

import java.util.ArrayList;

public class Task8 {

    public static void main(String[] args) {

        int[] numbers = {-3, 0, 3, 5, 10, 23, 36, 144, 21, 984, 257, -2};
        System.out.println(getPrimeNumbers(numbers));

    }

    public static ArrayList<Integer> getPrimeNumbers(int[] numbers) {

        ArrayList<Integer> primeNumbers = new ArrayList<>();

        for (int number : numbers) {

            int i = 0;
            int counter = 0;
            int divider = number;

            if (number < 1) continue;

            while (i < number) {

                if (number % divider == 0) {

                    counter++;
                }

                divider--;
                i++;

                if (counter > 3) break;

            }
            if (counter == 2) primeNumbers.add(number);

        }

        return primeNumbers;
    }
}

