import java.util.ArrayList;
import java.util.Scanner;

/*
Zadanie 11.
Napisz program który zamieni liczbę z systemu dziesiętnego na binarny np. 18 -> 10010
 */
public class Task33 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj proszę liczbę");
        int number = sc.nextInt();

        ArrayList<Integer> decimalToBinary = new ArrayList<>();

        if (number == 0) decimalToBinary.add(number);

        while (number != 0) {

            decimalToBinary.add(number % 2);
            number = number / 2;

        }
        for (int i = decimalToBinary.size() - 1; i >= 0; i--) {

            System.out.print(decimalToBinary.get(i));

        }
    }
}
