/*
Zadanie 8.
Zlicz wszystkie litery w podanym tekście np. Ala ma kota -> 9
 */
public class Task30 {

    public static void main(String[] args) {

        String sentence = "Ala ma kota";
        System.out.println(sentence.replaceAll(" ", "").length());
    }
}
