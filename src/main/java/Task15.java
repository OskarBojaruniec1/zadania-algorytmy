/*Zadanie 21.
Napisz metodę, która zwraca maksymalną długość 2 stringów.
maxStringLength(“aaaa”,”sad”) zwróci 4.
maxStringLength(“aaksadui”,”aaa”) zwróci 8.*/

public class Task15 {

    public static void main(String[] args) {

        System.out.println(maxStringLength("samolot", "pies"));

    }

    public static int maxStringLength(String a, String b) {

        return Math.max(a.length(), b.length());

    }
}
