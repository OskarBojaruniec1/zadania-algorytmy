import java.util.ArrayList;

/*
Zadanie 9.
Wypisz na ekran N wyrazów ciągu fibonacciego
 */
public class Task31 {

    public static void main(String[] args) {

        ArrayList<Integer> fibonacci = new ArrayList<>();
        int n = 20;

        int a = 0;
        int b = 1;

        for (int i = 0; i < n; i++) {

            int c = a + b;
            fibonacci.add(a);
            a = b;
            b = c;

        }
        System.out.println(fibonacci);
    }
}
