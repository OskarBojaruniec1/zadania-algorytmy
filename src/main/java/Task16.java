/*Zadanie 22.
Napisz funkcję, która odczytuje jako argument liczbę i wypisuje liczbę dzielników.*/

public class Task16 {

    public static void main(String[] args) {

        dividers(32);
    }

    public static void dividers(int number) {

        int divider = number;
        for (int i = 0; i < number; i++) {

            if (number % divider == 0) {

                System.out.println(divider);
            }
            divider--;

        }
    }
}
