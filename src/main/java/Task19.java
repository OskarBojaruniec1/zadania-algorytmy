/*Zadanie 25.
Skorzystaj z wcześniejszej funkcji (zad.11.) do znalezienia liczby od 2 do 10000,
która ma największą liczbę dzielników.*/

public class Task19 {

    public static void main(String[] args) {

        System.out.println(theHighestAmountOfDividers());

    }

    public static int theHighestAmountOfDividers() {

        int theHighestCounter = 0;
        int number = 0;

        for (int i = 2; i < 10001; i++) {

            int divider = i;
            int j = 0;
            int counter = 0;

            while (j < i) {

                if (i % divider == 0) counter++;
                j++;
                divider--;
            }
            if (counter > theHighestCounter) {

                theHighestCounter = counter;
                number = i;
            }
        }
        return number;
    }
}
