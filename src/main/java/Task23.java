/*
Zadanie 1.
Zaprogramuj algorytm znajdujący największy element z podanego ciągu liczb

int[] numbers = {1, 13, 51322, -513, 0, 53, 100, 555}
Największy element to 51322
*/

public class Task23 {

    public static void main(String[] args) {

        int[] numbers = {1, 13, 51322, -513, 0, 53, 100, 555};
        System.out.println(theHighestValue(numbers));

    }

    public static int theHighestValue(int[] numbers) {

        int max = numbers[0];

        for (int number : numbers) max = Math.max(max, number);

        return max;
    }
}
